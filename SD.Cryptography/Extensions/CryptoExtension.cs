﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SD.Cryptography.Interfaces;

namespace SD.Cryptography.Extensions
{
    public static class CryptoExtension
    {
        public static string Hash(this string text, IHasher hasher)
        {
            return hasher.Encrypt(text, Encoding.UTF8);
        }

        public static byte[] Hash(this IEnumerable<byte> data, IHasher hasher)
        {
            return hasher.Encrypt(data.ToArray());
        }
    }
}
