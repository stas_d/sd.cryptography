﻿namespace SD.Cryptography.Interfaces
{
    public interface IPasswordHasher : IHasher
    {
        bool IsValid(string data, string input);
    }
}
