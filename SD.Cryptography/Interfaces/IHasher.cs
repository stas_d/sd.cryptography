﻿using System.Text;

namespace SD.Cryptography.Interfaces
{
    public interface IHasher
    {
        string Encrypt(string text, Encoding enc = null);
        byte[] Encrypt(byte[] bytes);
    }
}