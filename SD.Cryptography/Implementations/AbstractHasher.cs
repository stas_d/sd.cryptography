﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using SD.Cryptography.Interfaces;

namespace SD.Cryptography.Implementations
{
    public abstract class AbstractHasher : IHasher
    {
        protected static string ByteToString(IEnumerable<byte> buff)
        {
            var sbinary = buff.Aggregate("", (current, t) => current + t.ToString("X2"));

            return sbinary;
        }

        public virtual string Encrypt(string text, Encoding enc = null)
        {
            if(enc == null)
                enc = Encoding.UTF8;

            return ByteToString(Encrypt(enc.GetBytes(text)));
        }
        public abstract byte[] Encrypt(byte[] bytes);
    }
}
