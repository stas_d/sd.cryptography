﻿using System.Security.Cryptography;
using System.Text;
using SD.Cryptography.Interfaces;

namespace SD.Cryptography.Implementations
{
    public class Md5HmacProvider : AbstractHasher
    {
        private readonly HMACMD5 _hasher;

        public override byte[] Encrypt(byte[] bytes)
        {
            return _hasher.ComputeHash(bytes);
        }

        public Md5HmacProvider(string key)
        {
            _hasher = new HMACMD5(Encoding.Default.GetBytes(key));
        }

        public Md5HmacProvider() : this("myKey")
        {
        }
    }
}
