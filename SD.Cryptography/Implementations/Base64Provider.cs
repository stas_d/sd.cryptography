﻿using System;
using System.Text;
using SD.Cryptography.Interfaces;

namespace SD.Cryptography.Implementations
{
    public class Base64Provider : AbstractHasher
    {
        public override byte[] Encrypt(byte[] bytes)
        {
            return Encoding.UTF8.GetBytes(Convert.ToBase64String(bytes));
        }
        public override string Encrypt(string text, Encoding enc = null)
        {
            if (enc == null)
                enc = Encoding.UTF8;

            return enc.GetString(Encrypt(enc.GetBytes(text)));
        }
    }
}
