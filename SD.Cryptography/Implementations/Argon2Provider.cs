﻿using System.Text;
using Konscious.Security.Cryptography;
using SD.Cryptography.Interfaces;

namespace SD.Cryptography.Implementations
{
    public class Argon2Provider : AbstractHasher
    {
        public override byte[] Encrypt(byte[] bytes)
        {

            var x = new Argon2d(bytes)
            {
                DegreeOfParallelism = 16,
                MemorySize = 8192,
                Iterations = 40
            };

            return x.GetBytes(bytes.Length < 8 ? 8 : bytes.Length);
        }
    }
}
