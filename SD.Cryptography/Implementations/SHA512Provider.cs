﻿using System.Security.Cryptography;
using System.Text;
using SD.Cryptography.Interfaces;

namespace SD.Cryptography.Implementations
{
    public class Sha512Provider : AbstractHasher
    {
        private readonly SHA512 _hasher = new SHA512Managed();

        public override byte[] Encrypt(byte[] bytes)
        {
            return _hasher.ComputeHash(bytes);
        }
    }
}
