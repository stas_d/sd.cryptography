﻿using System;
using System.Text;
using SD.Compression.Implementations;
using SD.Compression.Interfaces;
using SD.Cryptography.Interfaces;

namespace SD.Cryptography.Implementations
{
    public class SecurePasswordHasher : AbstractHasher, IPasswordHasher
    {
        private readonly string _key;

        private readonly IHasher _sha512 = new Sha512Provider();
        private readonly IHasher _crc32 = new Crc32Provider();
        private readonly IHasher _argon2 = new Argon2Provider();

        private readonly IHasher _sXor;


        private readonly IStringCompressionProvider _compression = new GZipCompressionProvider();
        public SecurePasswordHasher(string key)
        {
            _key = key;

            _sXor = new SimpleXorProvider(_key);
        }

        public override byte[] Encrypt(byte[] bytes)
        {
            var hashed = _crc32.Encrypt(bytes);

            hashed = _argon2.Encrypt(hashed);

            hashed = _sXor.Encrypt(hashed);

            hashed = _sha512.Encrypt(hashed);

            return hashed;
        }

        public bool IsValid(string data, string input)
        {
            return string.Equals(data, Encrypt(input), StringComparison.InvariantCulture);
        }
    }
}
