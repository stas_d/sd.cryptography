﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Crc32C;

namespace SD.Cryptography.Implementations
{
    public class Crc32Provider : AbstractHasher
    {
        private readonly Crc32CAlgorithm _hasher = new Crc32CAlgorithm(); 

        public override byte[] Encrypt(byte[] bytes)
        {
            return _hasher.ComputeHash(bytes);
        }
    }
}
