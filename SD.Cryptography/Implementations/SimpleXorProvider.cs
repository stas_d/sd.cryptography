﻿using System.Text;
using SD.Cryptography.Interfaces;

namespace SD.Cryptography.Implementations
{
    public class SimpleXorProvider : AbstractHasher
    {
        private readonly string _key;

        public SimpleXorProvider(string key)
        {
            _key = key;
        }

        public override byte[] Encrypt(byte[] bytes)
        {
            var sb = new byte[bytes.Length];

            for (var i = 0; i < bytes.Length; i++)
                sb[i] = (byte)(bytes[i] ^ _key[(i % _key.Length)]);

            return sb;
        }
    }
}
