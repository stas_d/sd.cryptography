﻿using System;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SD.Cryptography.Extensions;
using SD.Cryptography.Implementations;
using SD.Cryptography.Interfaces;

namespace SD.Test
{
    [TestClass]
    public class CryptoTest
    {
        // ReSharper disable once InconsistentNaming
        private const string _key = "superKey123Qwerty";
        // ReSharper disable once InconsistentNaming
        private const string _password = "qwerty";


        private readonly IHasher _md5Hmac = new Md5HmacProvider(_key);
        private readonly IHasher _sha512 = new Sha512Provider();
        private readonly IHasher _argon2 = new Argon2Provider();

        [TestMethod]
        public void RootShouldBeValid()
        {

            var provider = new SecurePasswordHasher(_key);

            var hashed = provider.Encrypt(_password, Encoding.UTF8);

            Assert.IsTrue(provider.IsValid(hashed, _password));
        }

        [TestMethod]
        public void RootShouldBeInvalidValid()
        {

            var provider = new SecurePasswordHasher("lol");
            var validProvider = new SecurePasswordHasher(_key);

            var hashed = validProvider.Encrypt(_password, Encoding.UTF8);

            Assert.IsFalse(provider.IsValid(hashed, _password));
        }


        [TestMethod]
        public void Md5HmacValid()
        {
            Assert.IsTrue(string.Equals("552c8443133fc2b54bdad5617470c7e9", _md5Hmac.Encrypt(_password), StringComparison.InvariantCultureIgnoreCase));
        }

        [TestMethod]
        public void Sha512Valid()
        {
            Assert.IsTrue(string.Equals("0DD3E512642C97CA3F747F9A76E374FBDA73F9292823C0313BE9D78ADD7CDD8F72235AF0C553DD26797E78E1854EDEE" +
                                        "0AE002F8ABA074B066DFCE1AF114E32F8",
                _sha512.Encrypt(_password)));
        }

        [TestMethod]
        public void Argon2Valid()
        {
            Assert.IsTrue(string.Equals("C3F4BE7D3ED5E984", _argon2.Encrypt(_password)));
        }

        [TestMethod]
        public void Base64Valid()
        {
            Assert.IsTrue(string.Equals("cXdlcnR5", "qwerty".Hash(new Base64Provider())));
        }
    }
}
