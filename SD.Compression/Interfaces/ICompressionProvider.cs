﻿namespace SD.Compression.Interfaces
{
    public interface IStringCompressionProvider
    {
        string Compress(string input);
        string Decompress(string input);
    }
}
